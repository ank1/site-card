<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('comments', static function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('news_id');
            $table->string('author');
            $table->mediumText('comment');
            $table->softDeletes();
            $table->timestamps();
        });

        // create FK
        Schema::table('comments', static function (Blueprint $table) {
            $table->foreign('news_id')->references('id')->on('news');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('comments');
    }
}
