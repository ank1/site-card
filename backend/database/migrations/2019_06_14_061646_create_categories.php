<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('categories', static function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
//            $table->integer('parent_id')->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->nestedSet();
        });

        // create FK
        Schema::table('categories', static function (Blueprint $table) {
            $table->foreign('parent_id')->references('id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('categories', static function (Blueprint $table) {
            $table->dropNestedSet();
        });
        Schema::dropIfExists('categories');
    }
}
