<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Auth routes
Route::group(['prefix' => 'auth'], static function () {
    Route::post('login', 'AuthController@login')->middleware('throttle:10,1');
    Route::group(['middleware' => 'jwt.auth'], static function () {
        Route::get('me', 'AuthController@me');
        Route::post('logout', 'AuthController@logout');
    });
});

// Frontend routes
Route::resource('news', 'NewsController');
Route::resource('comment', 'CommentController');
Route::resource('category', 'CategoryController');
