<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateNewsRequest;
use App\Http\Requests\UpdateNewsRequest;
use App\Models\Category;
use App\Models\News;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Str;

class NewsController extends Controller
{
    /**
     * NewsController constructor.
     */
    public function __construct()
    {
        $this->middleware('jwt.auth')->only(['store', 'update', 'destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return LengthAwarePaginator
     */
    public function index(): LengthAwarePaginator
    {
        $query = News::with('category');

        if (request()->sort === 'desc') {
            $query->latest();
        } else {
            $query->oldest();
        }

        return $query->paginate(3);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return JsonResponse
     */
    public function create(): JsonResponse
    {
        return response()->json([
            'categories' => Category::get()->toTree()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateNewsRequest $request
     * @return JsonResponse
     */
    public function store(CreateNewsRequest $request): JsonResponse
    {
        $news = new News();
        $news->author_id = auth()->id();
        $news->category_id = $request->category_id;
        $news->title = $request->title;
        $news->slug = Str::slug($request->title);
        $news->short_desc = $request->short_desc;
        $news->full_content = $request->full_content;
        $news->save();

        return response()->json(['success' => true]);
    }

    /**
     * Display the specified resource.
     *
     * @param $slug
     * @return News|Builder|Model|object|null
     */
    public function show($id)
    {
        return News::with(['category', 'comments'])->whereSlug($id)->first();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        return Category::latest()->get()->toTree();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateNewsRequest $request
     * @param $id
     * @return JsonResponse
     */
    public function update(UpdateNewsRequest $request, $id): JsonResponse
    {
        $news = News::findOrFail($id);
        $news->author_id = auth()->id();
        $news->category_id = $request->category_id;
        $news->title = $request->title;
        $news->slug = Str::slug($request->title);
        $news->short_desc = $request->short_desc;
        $news->full_content = $request->full_content;

        return response()->json(['success' => $news->save()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy($id): JsonResponse
    {
        News::findOrFail($id)->delete();

        return response()->json(['success' => true]);
    }
}
