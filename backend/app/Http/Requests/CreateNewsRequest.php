<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CreateNewsRequest
 * @property integer category_id
 * @property string short_desc
 * @property string title
 * @property string full_content
 * @package App\Http\Requests
 */
class CreateNewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'category_id' => 'required|integer|exists:categories,id',
            'title' => 'required|string|min:2|max:255',
            'short_desc' => 'required|string|min:2|max:1000',
            'full_content' => 'required|string|min:2|max:5000',
        ];
    }
}
