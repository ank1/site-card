<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property string category_name
 * @property integer parent_id
 * @property integer id
 * @property mixed category_id
 * @property mixed title
 * @property mixed short_desc
 * @property mixed full_content
 */
class UpdateNewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'category_id' => 'required|integer|exists:categories,id',
            'title' => 'required|string|min:2|max:255',
            'short_desc' => 'required|string|min:2|max:1000',
            'full_content' => 'required|string|min:2|max:5000',
        ];
    }
}
