<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CreateNewsRequest
 * @property mixed id
 * @property mixed name
 * @property mixed comment
 * @package App\Http\Requests
 */
class CreateCommentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'id' => 'required|integer|exists:news,id',
            'name' => 'required|string|min:1|max:255',
            'comment' => 'required|string|min:2|max:1000',
        ];
    }
}
