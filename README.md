**Installation**

Example: http://194.1.239.62:3000 (login: **login**, password: **password**)

1. Clone this repository
2. cd laradock
3. cp env-example .env
4. sh start.sh
5. sh workspace.sh
6. composer install && jwt:secret && art migrate
7. Exit form container (workspace)
8. cd ../frontend
9. npm run dev


![](https://i.imgur.com/MJopKPV.png)